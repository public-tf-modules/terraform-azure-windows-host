resource "azurerm_public_ip" "windows" {
  count = var.bastion ? 1 : 0
  name                = "${var.name}-windows-ip"
  location            = var.location
  resource_group_name = var.rg-name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "windows-nic" {
  name                = "${var.name}-nic"
  location            = var.location
  resource_group_name = var.rg-name

  ip_configuration {
    name                          = "${var.name}-internal-ip"
    subnet_id                     = var.subnet_id 
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = var.bastion ? azurerm_public_ip.windows[0].id : null
  }
}

resource "azurerm_windows_virtual_machine" "example" {
  name                = var.name
  resource_group_name = var.rg-name
  location            = var.location
  size                = "Standard_F2"
  admin_username      = var.username
  admin_password      = var.password
  network_interface_ids = [
    azurerm_network_interface.windows-nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}
